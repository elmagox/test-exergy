@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        Para generar un gráfico aleatorio por favor dirigirse a Menú > Graficar.
                    <div id="chart"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var char;
        $(function () {
            $("#graficar").click(function () {
               graficarHighcharts();
            });


            function graficarHighcharts() {
                chart = Highcharts.chart('chart', {
                    chart: {
                        type: 'spline',
                        events: {
                            load: requestData
                        }
                    },
                    title: {
                        text: 'Test Exergy'
                    },
                    xAxis: {
                        type: 'datetime',
                        tickPixelInterval: 150,
                        maxZoom: 20 * 1000
                    },
                    yAxis: {
                        minPadding: 0.2,
                        maxPadding: 0.2,
                        title: {
                            text: 'Value',
                            margin: 80
                        }
                    },
                    series: [{
                        name: 'Dato Aleatorio',
                        data: []
                    }]
                });
            }
        });

        function requestData() {
            $.ajax({
                url: '{{route('highchart')}}',
                success: function(point) {
                    var series = chart.series[0], shift = series.data.length > 20;
                    chart.series[0].addPoint(point, true, shift);
                },
                cache: false
            });
        }
    </script>
@endsection
