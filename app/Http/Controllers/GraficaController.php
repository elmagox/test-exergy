<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GraficaController extends Controller
{
    function graficaRandom(){
        $x = time() * 1000;
        $y = rand(0, 100);
        $ret = array($x, $y);
        return $ret;
    }
}
